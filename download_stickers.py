#!/usr/bin/env python3
import io
import json
import gzip
import shutil
import base64
import pathlib
import argparse

import telethon
import telethon.types
import telethon.functions
from telethon.sync import TelegramClient

import cv2
import lottie
import PIL.Image


def gather_data(client, config):
    data = config["packs"]

    print("Fetching data")

    with client:
        for name, pack in data.items():
            result = client(telethon.functions.messages.GetStickerSetRequest(
                telethon.types.InputStickerSetShortName(name),
                hash=0
            ))
            pack["title"] = result.set.title
            pack["short_name"] = result.set.short_name
            pack["url"] = "https://t.me/addstickers/" + result.set.short_name
            if result.set.animated:
                pack["type"] = "animated"
            elif result.set.videos:
                pack["type"] = "video"
            elif result.set.emojis:
                pack["type"] = "emoji"
            elif result.set.masks:
                pack["type"] = "mask"
            else:
                pack["type"] = "static"

            if "nsfw" not in pack:
                pack["nsfw"] = False
            if "generate_thumbnail" not in pack:
                pack["generate_thumbnail"] = True

            pack["thumb_version"] = result.set.thumb_version

            stickers = []
            pack["stickers"] = stickers
            emoji = {}
            for sticker in result.packs:
                for doc_id in sticker.documents:
                    emoji[doc_id] = sticker.emoticon

            for sticker in result.documents:
                stickers.append({
                    "emoji": emoji[sticker.id],
                    "id": sticker.id,
                    "access_hash": sticker.access_hash,
                    "file_reference": base64.b64encode(sticker.file_reference).decode("ascii"),
                    "mime_type": sticker.mime_type if sticker.mime_type != "application/x-tgsticker" else "application/json",
                })

    return data


def parse_config_path(path, context):
    if "$" not in path:
        return pathlib.Path(path)

    prefix = None
    chunks = path.split("/")
    if path.startswith("/") and "$" not in chunks[1]:
        chunks[1] = "/" + chunks[1]

    for chunk in chunks:
        if not chunk or chunk == ".":
            continue

        if chunk[0] == "$":
            chunk = context[chunk]

        if chunk == "..":
            prefix = prefix.parent
            continue

        if not prefix:
            if isinstance(chunk, str):
                prefix = pathlib.Path(chunk)
            else:
                prefix = chunk
        else:
            prefix /= chunk

    return prefix


def pack_path(pack, output_sfw, output_nsfw, output_context):
    return parse_config_path(output_nsfw if pack["nsfw"] else output_sfw, {**output_context, "$pack": pack["short_name"]})


def download_stickers(client, packs, data, output_sfw, output_nsfw, output_context):
    with client as client:
        for pack_name in packs:
            pack = data[pack_name]
            print("Downloading " + pack_name)
            out_path = pack_path(pack, output_sfw, output_nsfw, output_context)
            out_path.mkdir(parents=True, exist_ok=True)

            for sticker in pack["stickers"]:
                doc_id = sticker["id"]
                suffix = sticker["mime_type"].rsplit("/", 1)[1]

                file_path = out_path / ("%s.%s" % (doc_id, suffix))
                sticker["file"] = file_path
                sticker["suffix"] = suffix
                if not file_path.exists():
                    print("  %s" % sticker["emoji"])
                    doc = telethon.types.InputDocumentFileLocation(
                        doc_id,
                        access_hash=sticker["access_hash"],
                        file_reference=base64.b64decode(sticker["file_reference"]),
                        thumb_size=""
                    )

                    if suffix == "json":
                        file_bytes = client.download_file(doc, bytes)
                        inflated = gzip.decompress(file_bytes)
                        with open(file_path, "wb") as f:
                            f.write(inflated)

                    else:
                        client.download_file(doc, file_path)


            thumb_version = pack["thumb_version"]
            thumb_file = out_path / "thumb.webp"

            if pack["generate_thumbnail"]:
                print("  Thumbnail")
                if thumb_version is None:
                    first = pack["stickers"][0]

                    if first["suffix"] == "json":
                        animation = lottie.importers.core.import_lottie(str(first["file"]))
                        out = io.BytesIO()
                        frame = (animation.in_point + animation.out_point) / 2
                        lottie.exporters.cairo.export_png(animation, out, frame)
                        out.seek(0)
                        img = PIL.Image.open(out)
                        img.save(thumb_file)
                    elif first["suffix"] == "webp":
                        shutil.copy(first["file"], thumb_file)
                    elif first["suffix"] == "webm":
                        vidcap = cv2.VideoCapture(str(first["file"]))
                        success, image = vidcap.read()
                        cv2.imwrite(str(thumb_file), image)
                else:
                    input_thumb = telethon.types.InputStickerSetThumb(
                        telethon.types.InputStickerSetShortName(pack_name), thumb_version
                    )
                    client.download_file(input_thumb, thumb_file)



def stale_files(pack, output_sfw, output_nsfw, output_context):
    path = pack_path(pack, output_sfw, output_nsfw, output_context)
    used = {"thumb"}
    for sticker in pack["stickers"]:
        used.add(str(sticker["id"]))

    stale = []

    for file in path.iterdir():
        if file.is_file() and file.stem not in used:
            stale.append(file)

    return stale


src_path = pathlib.Path(__file__).absolute().parent

parser = argparse.ArgumentParser()
parser.add_argument("--api-id")
parser.add_argument("--api-hash")
parser.add_argument("--session", default="telegram_sticker_download")
parser.add_argument("--config", default=src_path / "dragon.best.json", type=pathlib.Path)
parser.add_argument("--cached", "-c", action="store_true", help="Use cached data")
parser.add_argument("--clean", action="store_true", help="Removes all existing data")
parser.add_argument("--download", nargs="+", help="Download images for the given packs (or `all` for everyone)")
parser.add_argument("--prune", "-p", action="store_true", help="Removes images that are no longer needed")

args = parser.parse_args()


with open(args.config) as f:
    config = json.load(f)


output_context = {
    "$src": src_path,
    "$pwd": pathlib.Path().absolute(),
}

for name, val in config["output"].items():
    if name[0] == "$":
        output_context[name] = parse_config_path(val, output_context)

output_sfw = config["output"]["sfw"]
output_nsfw = config["output"]["nsfw"]
path_data = parse_config_path(config["output"]["data"], output_context)
path_clean = parse_config_path(config["output"]["clean"], output_context)

if not args.cached:
    data = gather_data(TelegramClient(args.session, args.api_id, args.api_hash), config)
    path_data.parent.mkdir(parents=True, exist_ok=True)
    with open(path_data, "w") as f:
        json.dump(data, f, indent=4)

else:
    with open(path_data, "r") as f:
        data = json.load(f)

if args.clean:
    print("Clearing old data")
    shutil.rmtree(path_clean)

if args.download:
    if args.download == ["all"]:
        packs = list(data.keys())
    else:
        packs = args.download

    download_stickers(TelegramClient(args.session, args.api_id, args.api_hash), packs, data, output_sfw, output_nsfw, output_context)

if args.prune:
    for pack in data.values():
        print("Pruning %s" % pack["short_name"])
        to_prune = stale_files(pack, output_sfw, output_nsfw, output_context)
        if not to_prune:
            print("  Nothing to do")
        else:
            print("  %s files to remove" % len(to_prune))
            for file in to_prune:
                print("  %s" % file.name)
                file.unlink()
